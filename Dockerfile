FROM alpine:3.21.3
MAINTAINER horky@d3s.mff.cuni.cz
LABEL maintainer="horky@d3s.mff.cuni.cz"

ARG HUGO_VERSION
ARG HUGO_TARBALL="https://github.com/gohugoio/hugo/releases/download/v${HUGO_VERSION}/hugo_extended_${HUGO_VERSION}_Linux-64bit.tar.gz"

# Things that are installed:
#  - Hugo dependencies
#  - Node.js
#  - basic Python tools
RUN apk update \
    && apk add libstdc++ libc6-compat gcompat \
    && apk add curl \
    && apk add git rsync openssh \
    && apk add nodejs npm \
    && apk add python3 py3-virtualenv py-pip \
    && apk add gcc libc-dev python3-dev \
    && mkdir /root/hugo \
    && cd /root/hugo && curl -L "$HUGO_TARBALL" | tar xzf - \
    && mv /root/hugo/hugo /usr/bin/hugo \
    && cd / \
    && rm -rf /root/hugo \
    && rm -rf /var/cache/apk/* \
    && hugo version

CMD /bin/sh
